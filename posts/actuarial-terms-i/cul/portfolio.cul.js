import { profit, premiums_ as premiums_pp, claims_ as claims_pp, reserve_ as reserve_pp, Δreserve, adverse_reserve_ as adverse_reserve_pp, Δadverse_reserve, mclaims, mΔreserve, mΔadverse_reserve, premiums_less_claims, fut_profit_ as fut_profit_pp, fut_premiums_ as fut_premiums_pp, fut_claims_ as fut_claims_pp } from './policy.cul.js'
// export not necessary anymore

export const point_id = () => point_id_in; // should constrain start_year etc.

export const start_year = () => point_id() != undefined ? portfolio()[point_id()].start_year : 1
export const count = () => portfolio()[point_id()].count

//export const end_year = point_id() != undefined ? end_year_pp() : 50

export const portfolio = () => [
  {start_year:1, count:1},
  {start_year:2, count:1},
  {start_year:3, count:1},
  /*{start_year:4, count:1},
  {start_year:5, count:1},
  {start_year:6, count:1},
  {start_year:7, count:1},*/
]

export const premiums = () => {
  if (point_id() == undefined) {
    return portfolio().reduce((a,v,point_id_in) => a + count() * premiums_pp(), 0)
  }
  else return count() * premiums_pp();
}

export const claims = () => {
  if (point_id() == undefined) {
    return portfolio().reduce((a,v,point_id_in) => a + count() * claims_pp(), 0)
  }
  else return count() * claims_pp();
}

export const reserve = () => {
  if (point_id() == undefined) {
    return portfolio().reduce((a,v,point_id_in) => a + 1 * reserve_pp(), 0)
  }
  else return 1 * reserve_pp();
}

export const adverse_reserve = () => {
  if (point_id() == undefined) {
    return portfolio().reduce((a,v,point_id_in) => a + 1 * adverse_reserve_pp(), 0)
  }
  else return 1 * adverse_reserve_pp();
}

export const fut_profit = () => {
  if (point_id() == undefined) {
    return portfolio().reduce((a,v,point_id_in) => a + 1 * fut_profit_pp(), 0)
  }
  else return 1 * fut_profit_pp();
}
export const fut_premiums = () => {
  if (point_id() == undefined) {
    return portfolio().reduce((a,v,point_id_in) => a + 1 * fut_premiums_pp(), 0)
  }
  else return 1 * fut_premiums_pp();
}
export const fut_claims = () => {
  if (point_id() == undefined) {
    return portfolio().reduce((a,v,point_id_in) => a + 1 * fut_claims_pp(), 0)
  }
  else return 1 * fut_claims_pp();
}

// do i need to do this also for fut_x ??