export const year = () => year_in;
export const start_year = () => start_year_in
export const policy_year = () => year() - start_year() // 0 indexed
export const end_year = () => start_year() + term() - 1
export const term = () => 20 // derive end

export const premiums_fn = () => premiums_fn_in;
export const claims_fn = () => claims_fn_in;

export const premiums_pp = () => (year() < start_year() || year() > end_year()) ? 0 : premiums_fn()()[policy_year()+1]
export const claims_pp = () => (year() < start_year() || year() > end_year()) ? 0 : claims_fn()()[policy_year()+1]

export const premiums = () => {
  return sales().reduce((a,v,i) => a + v * premiums_pp({year_in:year()-i}), 0)
}
export const claims = () => {
  return sales().reduce((a,v,i) => a + v * claims_pp({year_in:year()-i}), 0)
}

export const premiums_less_claims = () => premiums() - claims();

export const fut_claims = () => {
  if (year() >= end_year()) return 0;
  else return fut_claims({ year_in: year() + 1 }) + claims({year_in: year() + 1})
}

export const fut_premiums = () => {
  if (year() >= end_year()) return 0;
  else return fut_premiums({ year_in: year() + 1 }) + premiums({year_in: year() + 1})
}

export const fut_profit = () => {
  if (year() >= end_year()) return 0;
  else return fut_profit({ year_in: year() + 1 }) + profit({year_in: year() + 1})
}


export const fut_bads = () => {
  if (year() >= end_year()) return 0;
  else return fut_bads({ year_in: year() + 1 }) + bads({year_in: year() + 1})
}

export const fut_not_bads = () => {
  if (year() >= end_year()) return 0;
  else return fut_not_bads({ year_in: year() + 1 }) + not_bads({year_in: year() + 1})
}

export const fut_positives = () => {
  if (year() >= end_year()) return 0;
  else return fut_positives({ year_in: year() + 1 }) + positives({year_in: year() + 1})
}

export const limit = () => limit_in; // for lever (not used)
export const bads = () => {
  /*if (profit({reserve_factor_in:0, adverse_reserve_factor_in:0}) < -limit()) CIRCULARITY
    return profit({reserve_factor_in:0, adverse_reserve_factor_in:0}) + limit()*/
  if (premiums() - claims() < -limit())
    return premiums() - claims() + limit()
  else return 0
}

export const not_bads = () => claims()-bads()



export const reserve_factor = () => reserve_factor_in ?? 1

export const positives = () => premiums() > claims() ? premiums() - claims() : 0;

export const reserve = () => {
  if (year() < start_year() - 1) return 0
  return Math.max(0, -fut_bads() -fut_positives() )
}

export const reserve_A = () => {
  if (year() < start_year() - 1) return 0
  return Math.max(0, -fut_bads() -fut_premiums()+fut_not_bads()-fut_claims() )
  //if (fut_premiums()-fut_not_bads()>-fut_bads()) return 0//-(fut_premiums()-fut_not_bads()-fut_bads());
  //else return -fut_bads()
  //return Math.max(0, -fut_premiums()+fut_claims()+fut_bads())
}

export const Δreserve = () => reserve() - reserve({year_in:year()-1})

//export const profit = () => premiums() - claims() - Δreserve()

export const adverse_reserve_factor = () => adverse_reserve_factor_in ?? .3
export const adverse_reserve = () => year() < start_year() - 1 ? 0 : adverse_reserve_factor()*fut_claims() // timing?

export const Δadverse_reserve = () => adverse_reserve() - adverse_reserve({year_in:year()-1})

export const profit = () => premiums() - claims() - Δreserve() - Δadverse_reserve()


// things I need to remove!
export const mclaims = () => -claims();
export const mΔreserve = () => -Δreserve();
export const mΔadverse_reserve = () => -Δadverse_reserve();


////

// NBS

export const sales = () => [1]; // not using this as reserves always at start


//export const capital_requirement = () => 
