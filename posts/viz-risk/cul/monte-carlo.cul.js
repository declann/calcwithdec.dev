import {
  retirement_fund_value,
  age,
  fund_value,
} from 'https://calculang.dev/models/taxes-pensions/pension-calculator.cul.js';


// must compile with memoization and must pass appropriate params into random calls, to ensure purity
// no longer works due to memo approach changes...

export const unit_growth_rate = () =>
  z0() * unit_growth_rate_std_dev() + unit_growth_rate_mean();
// vs. uniform:
//random({ age_in: age(), simulation_in: simulation() }) * 0.1;

// box-muller transform to generate z0 a standard normally distributed number
// https://en.wikipedia.org/wiki/Box%E2%80%93Muller_transform
export const u1 = () =>
  random({ random_key_in: `${age()}_${simulation()}_1_${random_seed()}` });
export const u2 = () =>
  random({ random_key_in: `${age()}_${simulation()}_2${random_seed()}` });
export const z0 = () =>
  Math.sqrt(-2.0 * Math.log(u1())) * Math.cos(2 * Math.PI * u2());
// todo use z1 of previous generation?
//export const z1 = () => Math.sqrt(-2.0 * Math.log(u1())) * Math.sin(2 * Math.PI * u2());


// alternative:
//export const random = () => Math.random();

export const simulation = () => simulation_in;

// todo scenario where these reduce
export const lifestyling = () => lifestyling_in
export const lifestyling_age = () => 60
export const lifestyling_on = () => lifestyling() && (age() > lifestyling_age())
// todo lifestyling_on
export const unit_growth_rate_std_dev = () => unit_growth_rate_std_dev_in//unit_growth_rate_mean()//lifestyling_on() ? 0 : ;
export const unit_growth_rate_mean = () => unit_growth_rate_mean_in//lifestyling_on() ? .03 : .08

// seeded random
import { seeded } from "https://cdn.jsdelivr.net/gh/declann/calculang-js-browser-dev@main/random.js"

//import { prng_alea } from 'https://cdn.jsdelivr.net/npm/esm-seedrandom/+esm'


export const random_seed = () => random_seed_in;
//const seeded = () => prng_alea("random_3324seed()", 2,4, random_key()/*+random_key()*/); // NOT RANDOM
  //random_key();

// SHOULDN'T CREATE A CUL_LINK (1,2,3 mitigates); todo look at issues
export const random = () => {
  return seeded(1,2,3) // I lost control of random seed, bring back?
  random_key()
}

export const random_key = () => random_key_in




// Working alternative (consistent with ll sim), note allowance in random_seed for the things within a seed which vary
// seeded random
/*import { prng_alea } from 'https://cdn.jsdelivr.net/npm/esm-seedrandom/+esm'
export const random_seed = () => random_seed_in + ' ' + age() + ' ' +simulation();
export const seeded = () => prng_alea(random_seed());
export const random = () => seeded({ random_seed_in: random_seed() })();
*/