import {empee_contribution_tax_relief, empee_contribution_cost, emper_contribution, fund_growth, contribution_charge, management_charge, check_cfs_vs_fund_value, retirement_age, age_0, missed_contribution_age, age} from './pension-calculator.cul.js'

export const age_1 = () => age_1_in //?? retirement_age();

// I want a fixed contribution amount: not proportional to salary
export const empee_contribution = () => {
  if (
    age() <= age_0() - 1 ||
    age() == retirement_age() ||
    age() == missed_contribution_age()
  )
    return 0;
  if (age() >= age_1()) return 0; // new condition to animate-in payments
  else return 1000; // fixed contribution amount: not proportional to salary 
};