---
title: "Hello world 🌎"
author:
  - name: "Declan"
    url: /about.html
date: "2023-07-26"
categories: [news]
---

Hey, I'm Declan 👋

I work with numbers! And here I'll write about [calculang](https://github.com/calculang/calculang) - a language for calculations, and other numbers subjects.

That includes finance things, AI, climate change, and other profound subjects where numbers have a real impact on us, as well as more fun stuff like maths in nature and around us, and maths art! 🎨

I'll slowly roll things out here, but for some of my earlier work along these lines you can check out my [collection on ObservableHQ](https://observablehq.com/collection/@declann/calculang).

That work is exclusively calculang, but on this blog I chose technology that lets me also write in better-established scientific computing languages Python, R and [Julia](https://www.nature.com/articles/d41586-019-02310-3) 🤝🌎

This is thanks to [Quarto](https://quarto.org), which also gives me a cute way to share the code inside my pages, for example:

```{python}
#| echo: true
print("🐍 says 'hello world'!") # This is Python!
```

🙌

Wouldn't it be nice if numbers had a very happy path like this to their workings? 🤔 Oh, lets not get far ahead of ourselves already!

So with the blog basically functional, hopefully I'll be able to share something mathsy and interesting... soon :)
