// some references:
// https://www.google.com/search?q=beating+heart+in+desmos
// https://www.youtube.com/watch?v=4rPUg2jmcOw

// my own desmos build to figure:
// https://www.desmos.com/calculator/hf4pefzsbl

export const x = ({ x_in }) => x_in;
export const waviness = ({ waviness_in }) => waviness_in;
export const tallness = ({ tallness_in }) => tallness_in;
export const radius = ({ radius_in }) => radius_in;
export const pinchiness = ({ pinchiness_in }) => pinchiness_in;

// override for simplifying out sine wave effect
export const use_wave_override = ({ use_wave_override_in }) => use_wave_override_in;
export const wave_override = ({ wave_override_in }) => wave_override_in;

export const trend = ({ x_in, pinchiness_in }) => Math.abs(x({ x_in })) ** pinchiness({ pinchiness_in });

export const wave = ({ use_wave_override_in, wave_override_in, waviness_in, x_in }) => {
  if (use_wave_override({ use_wave_override_in }) == 1) return wave_override({ wave_override_in });else
  return Math.sin(waviness({ waviness_in }) * Math.PI * x({ x_in }));
};

export const semi_circle = ({ x_in, radius_in }) => {
  if (Math.abs(x({ x_in })) > radius({ radius_in })) return 0;else
  return (radius({ radius_in }) ** 2 - x({ x_in }) ** 2) ** 0.5;
};

export const heart = ({ x_in, pinchiness_in, radius_in, use_wave_override_in, wave_override_in, waviness_in, tallness_in }) => trend({ x_in, pinchiness_in }) + semi_circle({ x_in, radius_in }) * wave({ use_wave_override_in, wave_override_in, waviness_in, x_in }) * tallness({ tallness_in });