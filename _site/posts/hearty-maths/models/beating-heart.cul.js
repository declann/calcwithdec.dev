// some references:
// https://www.google.com/search?q=beating+heart+in+desmos
// https://www.youtube.com/watch?v=4rPUg2jmcOw

// my own desmos build to figure:
// https://www.desmos.com/calculator/hf4pefzsbl

export const x = () => x_in;
export const waviness = () => waviness_in;
export const tallness = () => tallness_in;
export const radius = () => radius_in;
export const pinchiness = () => pinchiness_in;

// override for simplifying out sine wave effect
export const use_wave_override = () => use_wave_override_in;
export const wave_override = () => wave_override_in;

export const trend = () => Math.abs(x()) ** pinchiness();

export const wave = () => {
  if (use_wave_override() == 1) return wave_override();
  else return Math.sin(waviness() * Math.PI * x());
};

export const semi_circle = () => {
  if (Math.abs(x()) > radius()) return 0;
  else return (radius() ** 2 - x() ** 2) ** 0.5;
};

export const heart = () => trend() + semi_circle() * wave() * tallness();
