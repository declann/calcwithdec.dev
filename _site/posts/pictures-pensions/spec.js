const statey = false; // "highlighting" ?

import {range} from "https://cdn.jsdelivr.net/npm/underscore/+esm";

// statey signal works, but when off, we should be picking last input_cursor_id

// based on the spec in calculang.dev (modified)
export const spec = (width, y) => ({
  "$schema": "https://vega.github.io/schema/vega/v5.json",
  //"background": "#eef",
  "padding": 5,
  "width": Math.min(300, width-100),
  //"height": 500,
  "style": "cell",
  signals:  [
    {name: 'flash', value: 0, bindOFF: {input: 'range', min:0, max:1, step:0.01}},
    {name: 'flashall', value: 1, bindOFF: {input: 'range', min:0, max:1, step:0.01}},//
    {name: 'statey', value: false, bindOFF: {input: 'checkbox'}},
    {name: 'input_cursor_id_signal', value: 1, bindOFF: {input:'select', options:[0,1,2,3]}}, //update: "extent(pluck(data('data'), 'input_cursor_id'))"},
    {name: 'outflows_opacity', value: 1},
    {name: 'ydomain_x', value: 0}, // tween
    {name: 'ydomain_x0', update: "clamp(ydomain_x,0,1)"},
    {name: 'ydomain_start', update: "statey ? (extent(pluck(data('data_old'), 'value1_end'))) : (extent(pluck(data('data_2'), 'value1_end')))"},
    {name: 'ydomain_end', update: "extent(pluck(data('data_2'), 'value1_end'))"},
    {name: 'ydomain', update: "[ydomain_start[0]*(1-ydomain_x0)+ydomain_end[0]*ydomain_x0, ydomain_start[1]*(1-ydomain_x0)+ydomain_end[1]*ydomain_x0]"},//"isArray(ydomain_override) ? ydomain_override : ydomain_"},
    {"name": "offset", "value": 80},
    {"name": "cellHeight", "value": 100},
    {"name": "height", "update": "2 * (offset + cellHeight)"}
],

  "data": [
    {
      "name": "data",
      "values": [],
            "transform": [
              {type:'filter', expr: 'statey || datum.input_cursor_id==1'},
        {
          type: 'formula',
          //expr: 'datum.value >=0', This is bloody hard to debug and do correctly, and must be correct for posneg swaps etc. So just banning negative charges for now and will fix this later!
          expr: 'indexof(["management_charge","contribution_charge"],datum.formula) == -1',
          as: 'posneg'
        },
        {
          type: 'formula', expr: 'abs(datum.value)', as: 'value'
        },
        {
          "type": "stack",
          "groupby": ["age_in"],
          "field": "value",
          "sort": {"field": ["formula", "input_cursor_id"], "order": ["ascending","descending"]},
          "as": ["value1_start", "value1_end"],
          "offset": "zero"
        },
      ]
    },
    {
      name: 'data2',
      source: 'data',
                  "transform": [
        {
          type: 'filter',
          expr: `datum.input_cursor_id == input_cursor_id_signal`
        },
                  ]},
                  {
                    name: 'data_old',
                    source: 'data',
                                "transform": [
                      {
                        type: 'filter',
                        expr: `datum.input_cursor_id == input_cursor_id_signal-1 && statey`, // eliminates chk
                      },
                            
                      {
                        "type": "stack",
                        "groupby": ["age_in"],
                        "field": "value",
                        "sort": {"field": ["formula", "input_cursor_id"], "order": ["ascending","descending"]},
                        "as": ["value1_start", "value1_end"],
                        "offset": "zero"
                      },
              ]},
              {
                name: 'data_2',
                source: 'data',
                            "transform": [
                  {
                    type: 'filter',
                    expr: `datum.input_cursor_id == input_cursor_id_signal`,
                  },
                        
                  {
                    "type": "stack",
                    "groupby": ["age_in"],
                    "field": "value",
                    "sort": {"field": ["formula", "input_cursor_id"], "order": ["ascending","descending"]},
                    "as": ["value1_start", "value1_end"],
                    "offset": "zero"
                  },
          ]}
    
  ],
  scales: [    {
      "name": "gscale",
      "type": "band",
      "range": [0, {"signal": "height"}],
      "round": true,
      "domain": {
        "data": "data",
        "field": "posneg",
        /*"sort": {
          "field": "posneg",
          "op": "median",
          "order": "descending"
        }*/
      }
    },
           
    {
      "name": "y_shared",
      "type": "linear",
      domainMin: '0',// domainMax: '60000', // doesn't generalise
      "domain": {"signal": "ydomain"},//{"data": statey?"data_old" : 'data', "fields": ["value1_start", "value1_end"]}, // give this a cushion ??
      "range": [{"signal": "cellHeight"}, 0],
      "nice": false,
      "zero": true,
      
    },
           
    {
      "name": "y_bad", // how to share y scale on the stack total ?!
      "type": "linear",
      domainMin: '0', domainMax: {signal: "domain('yyy')[1]*2"}, // doesn't generalise. Can I not x2 or update when dragging?
      "domain": {"data": "data", "fields": ["value1_start", "value1_end"]}, // give this a cushion ??
      "range": [{"signal": "cellHeight"}, 0],
      "nice": false,
      "zero": true,
      
    },
           
    {
      "name": "color",
      "type": "ordinal",
      "domain": {"data": "data", "field": "formula", "sort": ["empee_contribution_cost","check_cfs_vs_fund_value"]},
      "range": "category"
    },
],
  "marks": [
    {
      type: 'group',
      name: 'posneg_dummy', // using dummy so I can place header text "inflows" "outflows" UNDERNEATH impact marks
      // I get an unidentified dataset if refer to a dataset placed before the group mark that creates it
      from: {
        facet: {
          data: 'data',
          name: 'posnegs',
          groupby: 'posneg'
        }
      },
            "encode": {
        "enter": {
          "y": {"scale": "gscale", "field": "posneg", "offset": {"signal": "offset"}},
          "height": {"signal": "cellHeight"},
          "width": {"signal": "width"},
          "stroke": {"value": null}
        }
      },
    },
    ,
      
    {
      "type": "text",
      "from": {"data": "posneg_dummy"},
      
      "encode": {
        "enter": {
          "x": {"field": "width", "mult": 0.5},
          "y": {"field": "y", offset:-25},
          "fontSize": {"value": 31},
          opacity: {value: 1},
          //"fontWeight": {"value": "bold"},
          "text": {"signal": "datum.datum.posneg ? 'Inflows 💰' : 'Outflows 💸' "},
          "align": {"value": "center"},
          "baseline": {"value": "top"},
          "fill": {"value": "#555"}
        }
      }
    },
    {
      type: 'group',
      name: 'posneg',
      from: {
        facet: {
          data: 'data',
          name: 'posnegs',
          groupby: 'posneg'
        }
      },
    data: [{name: 'data_windowed', source:'posnegs', transform: [
              { // See newer keep_add_subtract
          "type": "window",
          "params": [null],
          "as": ["B_value"],
          "ops": ["lag"],
          "fields": ["value"],
          "sort": {
            "field": ["input_cursor_id"],
            "order": ["descending"]
          },
          "groupby": ["formula", "age_in"],
          "frame": [-1, 0]
        },
        {
          "type": "formula",
          "expr": "datum.value-datum.B_value",
          "as": "impact"
        },
        {"type": "formula", "expr": `datum.value > datum.B_value ? 0 : datum.B_value - datum.value`, "as": "adj"}, // I need to record the adj in id-1 for use in calc for id w. a follow-up window fn !
        {"type": "formula", "expr": `datum.input_cursor_id == input_cursor_id_signal-1 ? datum.value > datum.B_value ? datum.value - datum.B_value : datum.B_value - datum.value : datum.value`, "as": "BplusAminusB"}, // I need to record the adj in id-1 for use in calc for id w. a follow-up window fn !
                {"type": "formula", "expr": `datum.adj`, "as": "adjposneg"},

        {
          "type": "window",
          "params": [null],
          "as": ["B_adj"],
          "ops": ["lag"],
          "fields": ["adj"],
          "sort": {
            "field": ["input_cursor_id"],
            "order": ["ascending"]
          },
          "groupby": ["formula", "age_in"],
          "frame": [-1, 0]
        },
        {
          "type": "formula",
          "expr": "datum.BplusAminusB-datum.B_adj",
          "as": "BplusAminusB"
        },
        {"type": "filter", "expr": "statey ? abs(datum.BplusAminusB)>=0.01 : 1"}, // TODO statey

    ]}, 
    {
      "name": "data_0",
      "source": "data_windowed",
      "transform": [
        {
          "type": "stack",
          "groupby": ["age_in"],
          "field": "BplusAminusB",
          "sort": {"field": ["formula", "input_cursor_id"], "order": ["ascending","descending"]},
          "as": ["value_start", "value_end"],
          "offset": "zero"
        },
        { type: 'formula', 'expr': 'abs(datum.value_end)', as: 'abs_value_end'},
        { type: 'formula', 'expr': 'abs(datum.value_start)', as: 'abs_value_start'},
        /*{
          "type": "filter",
          "expr": "isValid(datum[\"age_in\"]) && isFinite(+datum[\"age_in\"]) && isValid(datum[\"value\"]) && isFinite(+datum[\"value\"])"
        }*/
      ]
    }],
            "encode": {
        "enter": {
          "y": {"scale": "gscale", "field": "posneg", "offset": {"signal": "offset"}},
          "height": {"signal": "cellHeight"},
          "width": {"signal": "width"},
          "stroke": {"value": null}
        }
      },
      marks: [


        {
          "name": "marks",
          "type": "rect",
          "style": ["bar"],
          "from": {"data": "data_0"},
          "encode": {
            "update": {
              "stroke": { "signal": `datum.posneg ? (datum.impact > 0 ? 'red' : 'green') : (datum.impact > 0 ? 'green' : 'red')`},
              //"stroke": { "signal": `datum.posneg ? (datum.input_cursor_id == ${input_cursor_id} ? 100 : -100) : (datum.input_cursor_id == ${input_cursor_id} ? -100 : 100)`},
              "strokeOpacity": {"value": 1},
              "tooltip": {
                "signal": "{formula: datum['formula'], value: format(datum.value, ',.2f'), Age: datum['age_in']}"
              },
              "fill": {"scale": "color", "field": "formula"},
              //"opacity": {"scale": "opacity", "field": "input_cursor_id"},
              opacity: {signal: "(1-(flash+flashall)*.8)*(datum.input_cursor_id == input_cursor_id_signal ? (statey ? 0.3 : 1) : 1)"},
    
                  //{ name: 'opacity', type: 'ordinal', "domain": {signal: "[input_cursor_id_signal-1 , input_cursor_id_signal]"}  , range: [1,statey ? 0.3 : 1]},
    
                  //    //{ name: 'strokeWidth', type: 'ordinal', "domain": {signal: "[input_cursor_id_signal, input_cursor_id_signal-1]"}  , range: [0,2]},
    
              //"strokeWidth": {"scale": "strokeWidth", "field": "input_cursor_id"},
              strokeWidth: {signal: "datum.input_cursor_id == input_cursor_id_signal ? 0 : (statey ? 2 : 0)"},
    
              "ariaRoleDescription": {"value": "bar"},
              "description": {
                "signal": "\"age_in: \" + (format(datum[\"age_in\"], \"\")) + \"; value: \" + (format(datum[\"value\"], \"\")) + \"; formula: \" + (isValid(datum[\"formula\"]) ? datum[\"formula\"] : \"\"+datum[\"formula\"])"
              },
              "x": {"scale": "x", "field": "age_in"},
              "width": {"signal": "max(0.25, bandwidth('x'))"},
              "y": {"scale": y, "field": "abs_value_end"},
              "y2": {"scale": y, "field": "abs_value_start"}
            }
          }
        },

        {
          "name": "flashes",
          "type": "rect",
          "style": ["bar"],
          "from": {"data": "data_0"},
          "encode": {
            "update": {
              "stroke": { "signal": `'white'`},
              //"stroke": { "signal": `datum.posneg ? (datum.input_cursor_id == ${input_cursor_id} ? 100 : -100) : (datum.input_cursor_id == ${input_cursor_id} ? -100 : 100)`},
              "strokeOpacity": {"value": 1},
              //"fill": {"scale": "color", "field": "formula"},
              "fill": {"signal": "datum.formula == 'empee_contribution_cost' ? 'crimson' : (datum.posneg ? 'lawngreen' : 'red')"},
              //"opacity": {"scale": "opacity", "field": "input_cursor_id"},
              opacity: {signal: "(datum.formula == 'empee_contribution_cost' ? flash : 0) + flashall"},
              //opacity: {signal: "datum.input_cursor_id == input_cursor_id_signal ? (statey ? 0.3 : 1) : 1"},
    
                  //{ name: 'opacity', type: 'ordinal', "domain": {signal: "[input_cursor_id_signal-1 , input_cursor_id_signal]"}  , range: [1,statey ? 0.3 : 1]},
    
                  //    //{ name: 'strokeWidth', type: 'ordinal', "domain": {signal: "[input_cursor_id_signal, input_cursor_id_signal-1]"}  , range: [0,2]},
    
              //"strokeWidth": {"scale": "strokeWidth", "field": "input_cursor_id"},
              strokeWidth: {signal: "datum.input_cursor_id == input_cursor_id_signal ? 0 : 10"},
    
              "x": {"scale": "x", "field": "age_in"},
              "width": {"signal": "max(0.25, bandwidth('x'))"},
              "y": {"scale": y, "field": "abs_value_end"},
              "y2": {"scale": y, "field": "abs_value_start"}
            }
          }
        }
  ],
  "scales": [    {
      "name": "y",
      "type": "linear",
      domainMin: '0',// domainMax: '60000', // doesn't generalise
      "domain": {"data": 'data_0', "fields": ["abs_value_start", "abs_value_end"]}, // give this a cushion ??
      "range": [{"signal": "cellHeight"}, 0],
      "nice": false,
      "zero": true,
      
    },
    {
      "name": "x",
      "type": "band",
      "domain": {"data": "data_0", "field": "age_in", "sort": true},
      //domain: {signal: "(extent(pluck(data('data_0'), 'age_in')))"},//_.range(40,65.1),
      "range": [0, {"signal": "width"}],
      "paddingInner": 0.2,
      "paddingOuter": 0.1
    },
    //{ name: 'opacity', type: 'ordinal', "domain": {signal: "[input_cursor_id_signal-1 , input_cursor_id_signal]"}  , range: [1,statey ? 0.3 : 1]},
    //{ name: 'strokeWidth', type: 'ordinal', "domain": {signal: "[input_cursor_id_signal, input_cursor_id_signal-1]"}  , range: [0,2]},
{
  "name": "threshold",
  "type": "threshold",
  "domain": [-99999999, 1],
  "range": ["green", "red", "green"]
} 
  ],
  "axes": [
    {
      "scale": "x",
      "orient": "bottom",
      "grid": false,
      //"title": "age_in",
      //"labelAlign": "center",
      //"labelAngle": -10,
      "labelBaseline": "top",
      "zindex": 0,
      ticks: true,
      values: range(15,105,5)
    },
    {
      "scale": y,
      "orient": "left",
      "grid": true,
      gridOpacity: 0.2,
      format: ".0s",
      //labelAngle:-90,
      "title": `Δ fund value`, // making this always show as delta
      "labelOverlap": true,
      "tickCount": 2,
      "zindex": 0
    }
    ],
    },
    {type: 'rect',
      encode: {
        enter: {
          x: { value: -50},
          y: { signal: "cellHeight+offset+50"},
          width: { signal: "width+50"},
          height: {signal: "cellHeight+50"},
          "fill": {"value": "white"},
          "stroke": {"value": "white"},
        },
        update: {
          opacity: {signal: "1-outflows_opacity"}
        }
      }
    },{
      "type": "text",
      "from": {"data": "posneg_dummy"},
      
      "encode": {
        "enter": {
          "x": {"field": "width", "mult": 0.5},
          "y": {"field": "y", offset:65},
          "fontSize": {"value": 31},
          //"fontWeight": {"value": "bold"},
          "text": {"signal": "datum.datum.posneg ? '➕' : '➖' "},
          "align": {"value": "center"},
          "baseline": {"value": "top"},
          "fill": {"value": "#555"}
        },
        update: {
          opacity: {signal: "min(0.8,flashall*outflows_opacity)"},
        }
      }
    }],
       // "legends": [{"fill": "color", "symbolType": "square", "title": "formula"}],
         "background": "white",
         stroke: null,
  "group": {
    "fill": "#dedede",
    stroke: null
  }
,
  "config": {
      "style": {
    "cell": {
      "stroke": "transparent"
    }
  }
,
    view: {stroke: null}
    //"legend": {"orient": "bottom", "layout": {"right": {"anchor": "middle"}}}
  }

    })