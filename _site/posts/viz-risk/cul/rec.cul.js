import {
  retirement_fund_value,
  age,
  fund_value,
  unit_growth_rate_ as actual_unit_growth_rates,
  unit_growth_rate_mean
} from './monte-carlo.cul.js';

export const actual_unit_growth_rates_co = () => actual_unit_growth_rates_co_in

export const unit_growth_rate = () => {
  if (age() > actual_unit_growth_rates_co())
    return unit_growth_rate_mean()
  else
    return actual_unit_growth_rates()
}
